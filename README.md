# Isoquery

[![Build Status](https://ci.codeberg.org/api/badges/toddy/isoquery/status.svg)](https://ci.codeberg.org/toddy/isoquery)
[![builds.sr.ht status](https://builds.sr.ht/~toddy/isoquery.svg)](https://builds.sr.ht/~toddy/isoquery?)
[![Translation status](https://hosted.weblate.org/widgets/isoquery/-/manpage/svg-badge.svg)](https://hosted.weblate.org/engage/isoquery/)

Search and display ISO codes for countries, languages, currencies, and scripts.

Isoquery can be used to generate a tabular output of the ISO standard
codes provided by the package iso-codes. It parses the JSON files and shows
all included ISO codes or just matching entries, if specified on the command
line. Moreover, it's possible to get all available translations for
the ISO standard.

## Dependencies

In order to build the program from source, you need asciidoctor, po4a,
and gettext.

It's also very useful to have iso-codes installed, although this is
not strictly necessary.

## Installation

Just run "./configure && make install".

## Translation

The program and the manpage can be translated using Weblate,
see https://hosted.weblate.org/projects/isoquery/ for details.

## Author and Copyright

Copyright © Dr. Tobias Quathamer <toddy@debian.org>

## License

This program is released under the GNU General Public License version 3,
see COPYING for details.
