DE-BB	Land		Brandenburg
DE-BE	Land		Berlin
DE-BW	Land		Baden-Württemberg
DE-BY	Land		Bayern
DE-HB	Land		Bremen
DE-HE	Land		Hessen
DE-HH	Land		Hamburg
DE-MV	Land		Mecklenburg-Vorpommern
DE-NI	Land		Niedersachsen
DE-NW	Land		Nordrhein-Westfalen
DE-RP	Land		Rheinland-Pfalz
DE-SH	Land		Schleswig-Holstein
DE-SL	Land		Saarland
DE-SN	Land		Sachsen
DE-ST	Land		Sachsen-Anhalt
DE-TH	Land		Thüringen
ES-A	Province	VC	Alacant*
ES-AB	Province	CM	Albacete
ES-AL	Province	AN	Almería
ES-AN	Autonomous community		Andalucía
ES-AR	Autonomous community		Aragón
ES-AS	Autonomous community		Asturias, Principado de
ES-AV	Province	CL	Ávila
ES-B	Province	CT	Barcelona [Barcelona]
ES-BA	Province	EX	Badajoz
ES-BI	Province	PV	Bizkaia
ES-BU	Province	CL	Burgos
ES-C	Province	GA	A Coruña [La Coruña]
ES-CA	Province	AN	Cádiz
ES-CB	Autonomous community		Cantabria
ES-CC	Province	EX	Cáceres
ES-CE	Autonomous city in north africa		Ceuta
ES-CL	Autonomous community		Castilla y León
ES-CM	Autonomous community		Castilla-La Mancha
ES-CN	Autonomous community		Canarias
ES-CO	Province	AN	Córdoba
ES-CR	Province	CM	Ciudad Real
ES-CS	Province	VC	Castelló*
ES-CT	Autonomous community		Catalunya [Cataluña]
ES-CU	Province	CM	Cuenca
ES-EX	Autonomous community		Extremadura
ES-GA	Autonomous community		Galicia [Galicia]
ES-GC	Province	CN	Las Palmas
ES-GI	Province	CT	Girona [Gerona]
ES-GR	Province	AN	Granada
ES-GU	Province	CM	Guadalajara
ES-H	Province	AN	Huelva
ES-HU	Province	AR	Huesca
ES-IB	Autonomous community		Illes Balears [Islas Baleares]
ES-J	Province	AN	Jaén
ES-L	Province	CT	Lleida [Lérida]
ES-LE	Province	CL	León
ES-LO	Province	RI	La Rioja
ES-LU	Province	GA	Lugo [Lugo]
ES-M	Province	MD	Madrid
ES-MA	Province	AN	Málaga
ES-MC	Autonomous community		Murcia, Región de
ES-MD	Autonomous community		Madrid, Comunidad de
ES-ML	Autonomous city in north africa		Melilla
ES-MU	Province	MC	Murcia
ES-NA	Province	NC	Nafarroa*
ES-NC	Autonomous community		Nafarroako Foru Komunitatea*
ES-O	Province	AS	Asturias
ES-OR	Province	GA	Ourense [Orense]
ES-P	Province	CL	Palencia
ES-PM	Province	IB	Illes Balears [Islas Baleares]
ES-PO	Province	GA	Pontevedra [Pontevedra]
ES-PV	Autonomous community		Euskal Herria
ES-RI	Autonomous community		La Rioja
ES-S	Province	CB	Cantabria
ES-SA	Province	CL	Salamanca
ES-SE	Province	AN	Sevilla
ES-SG	Province	CL	Segovia
ES-SO	Province	CL	Soria
ES-SS	Province	PV	Gipuzkoa
ES-T	Province	CT	Tarragona [Tarragona]
ES-TE	Province	AR	Teruel
ES-TF	Province	CN	Santa Cruz de Tenerife
ES-TO	Province	CM	Toledo
ES-V	Province	VC	Valencia
ES-VA	Province	CL	Valladolid
ES-VC	Autonomous community		Valenciana, Comunidad
ES-VI	Province	PV	Araba*
ES-Z	Province	AR	Zaragoza
ES-ZA	Province	CL	Zamora
FR-01	Metropolitan department	ARA	Ain
FR-02	Metropolitan department	HDF	Aisne
FR-03	Metropolitan department	ARA	Allier
FR-04	Metropolitan department	PAC	Alpes-de-Haute-Provence
FR-05	Metropolitan department	PAC	Hautes-Alpes
FR-06	Metropolitan department	PAC	Alpes-Maritimes
FR-07	Metropolitan department	ARA	Ardèche
FR-08	Metropolitan department	GES	Ardennes
FR-09	Metropolitan department	OCC	Ariège
FR-10	Metropolitan department	GES	Aube
FR-11	Metropolitan department	OCC	Aude
FR-12	Metropolitan department	OCC	Aveyron
FR-13	Metropolitan department	PAC	Bouches-du-Rhône
FR-14	Metropolitan department	NOR	Calvados
FR-15	Metropolitan department	ARA	Cantal
FR-16	Metropolitan department	NAQ	Charente
FR-17	Metropolitan department	NAQ	Charente-Maritime
FR-18	Metropolitan department	CVL	Cher
FR-19	Metropolitan department	NAQ	Corrèze
FR-20R	Metropolitan collectivity with special status		Corse
FR-21	Metropolitan department	BFC	Côte-d'Or
FR-22	Metropolitan department	BRE	Côtes-d'Armor
FR-23	Metropolitan department	NAQ	Creuse
FR-24	Metropolitan department	NAQ	Dordogne
FR-25	Metropolitan department	BFC	Doubs
FR-26	Metropolitan department	ARA	Drôme
FR-27	Metropolitan department	NOR	Eure
FR-28	Metropolitan department	CVL	Eure-et-Loir
FR-29	Metropolitan department	BRE	Finistère
FR-2A	Metropolitan department	20R	Corse-du-Sud
FR-2B	Metropolitan department	20R	Haute-Corse
FR-30	Metropolitan department	OCC	Gard
FR-31	Metropolitan department	OCC	Haute-Garonne
FR-32	Metropolitan department	OCC	Gers
FR-33	Metropolitan department	NAQ	Gironde
FR-34	Metropolitan department	OCC	Hérault
FR-35	Metropolitan department	BRE	Ille-et-Vilaine
FR-36	Metropolitan department	CVL	Indre
FR-37	Metropolitan department	CVL	Indre-et-Loire
FR-38	Metropolitan department	ARA	Isère
FR-39	Metropolitan department	BFC	Jura
FR-40	Metropolitan department	NAQ	Landes
FR-41	Metropolitan department	CVL	Loir-et-Cher
FR-42	Metropolitan department	ARA	Loire
FR-43	Metropolitan department	ARA	Haute-Loire
FR-44	Metropolitan department	PDL	Loire-Atlantique
FR-45	Metropolitan department	CVL	Loiret
FR-46	Metropolitan department	OCC	Lot
FR-47	Metropolitan department	NAQ	Lot-et-Garonne
FR-48	Metropolitan department	OCC	Lozère
FR-49	Metropolitan department	PDL	Maine-et-Loire
FR-50	Metropolitan department	NOR	Manche
FR-51	Metropolitan department	GES	Marne
FR-52	Metropolitan department	GES	Haute-Marne
FR-53	Metropolitan department	PDL	Mayenne
FR-54	Metropolitan department	GES	Meurthe-et-Moselle
FR-55	Metropolitan department	GES	Meuse
FR-56	Metropolitan department	BRE	Morbihan
FR-57	Metropolitan department	GES	Moselle
FR-58	Metropolitan department	BFC	Nièvre
FR-59	Metropolitan department	HDF	Nord
FR-60	Metropolitan department	HDF	Oise
FR-61	Metropolitan department	NOR	Orne
FR-62	Metropolitan department	HDF	Pas-de-Calais
FR-63	Metropolitan department	ARA	Puy-de-Dôme
FR-64	Metropolitan department	NAQ	Pyrénées-Atlantiques
FR-65	Metropolitan department	OCC	Hautes-Pyrénées
FR-66	Metropolitan department	OCC	Pyrénées-Orientales
FR-67	Metropolitan department	GES	Bas-Rhin
FR-68	Metropolitan department	GES	Haut-Rhin
FR-69	Metropolitan department	ARA	Rhône
FR-70	Metropolitan department	BFC	Haute-Saône
FR-71	Metropolitan department	BFC	Saône-et-Loire
FR-72	Metropolitan department	PDL	Sarthe
FR-73	Metropolitan department	ARA	Savoie
FR-74	Metropolitan department	ARA	Haute-Savoie
FR-75	Metropolitan department	IDF	Paris
FR-76	Metropolitan department	NOR	Seine-Maritime
FR-77	Metropolitan department	IDF	Seine-et-Marne
FR-78	Metropolitan department	IDF	Yvelines
FR-79	Metropolitan department	NAQ	Deux-Sèvres
FR-80	Metropolitan department	HDF	Somme
FR-81	Metropolitan department	OCC	Tarn
FR-82	Metropolitan department	OCC	Tarn-et-Garonne
FR-83	Metropolitan department	PAC	Var
FR-84	Metropolitan department	PAC	Vaucluse
FR-85	Metropolitan department	PDL	Vendée
FR-86	Metropolitan department	NAQ	Vienne
FR-87	Metropolitan department	NAQ	Haute-Vienne
FR-88	Metropolitan department	GES	Vosges
FR-89	Metropolitan department	BFC	Yonne
FR-90	Metropolitan department	BFC	Territoire de Belfort
FR-91	Metropolitan department	IDF	Essonne
FR-92	Metropolitan department	IDF	Hauts-de-Seine
FR-93	Metropolitan department	IDF	Seine-Saint-Denis
FR-94	Metropolitan department	IDF	Val-de-Marne
FR-95	Metropolitan department	IDF	Val-d'Oise
FR-971	Overseas department	GP	Guadeloupe
FR-972	Overseas department	MQ	Martinique
FR-973	Overseas department	GF	Guyane (française)
FR-974	Overseas department	RE	La Réunion
FR-976	Overseas department	YT	Mayotte
FR-ARA	Metropolitan region		Auvergne-Rhône-Alpes
FR-BFC	Metropolitan region		Bourgogne-Franche-Comté
FR-BL	Overseas collectivity		Saint-Barthélemy
FR-BRE	Metropolitan region		Bretagne
FR-CP	Dependency		Clipperton
FR-CVL	Metropolitan region		Centre-Val de Loire
FR-GES	Metropolitan region		Grand-Est
FR-GF	Overseas region		Guyane (française)
FR-GP	Overseas region		Guadeloupe
FR-HDF	Metropolitan region		Hauts-de-France
FR-IDF	Metropolitan region		Île-de-France
FR-MF	Overseas collectivity		Saint-Martin
FR-MQ	Overseas region		Martinique
FR-NAQ	Metropolitan region		Nouvelle-Aquitaine
FR-NC	Overseas collectivity with special status		Nouvelle-Calédonie
FR-NOR	Metropolitan region		Normandie
FR-OCC	Metropolitan region		Occitanie
FR-PAC	Metropolitan region		Provence-Alpes-Côte-d’Azur
FR-PDL	Metropolitan region		Pays-de-la-Loire
FR-PF	Overseas collectivity		Polynésie française
FR-PM	Overseas collectivity		Saint-Pierre-et-Miquelon
FR-RE	Overseas region		La Réunion
FR-TF	Overseas territory		Terres australes françaises
FR-WF	Overseas collectivity		Wallis-et-Futuna
FR-YT	Overseas region		Mayotte
NO-03	County		Oslo
NO-11	County		Rogaland
NO-15	County		Møre og Romsdal
NO-18	County		Nordland
NO-21	Arctic region		Svalbard (Arctic Region)
NO-22	Arctic region		Jan Mayen (Arctic Region)
NO-30	County		Viken
NO-34	County		Innlandet
NO-38	County		Vestfold og Telemark
NO-42	County		Agder
NO-46	County		Vestland
NO-50	County		Trööndelage
NO-54	County		Romssa ja Finnmárkku
