# Authors of Isoquery

- Dr. Tobias Quathamer <toddy@debian.org>

# Program translators

- Dr. Tobias Quathamer <toddy@debian.org> (German)
- Christian Perrier <bubulle@debian.org> (French)
- Yuri Kozlov <yuray@komyakino.ru> (Russian)
- Martin Bagge / brother <brother@bsnet.se> (Swedish)
- Michal Simunek <michal.simunek@gmail.com> (Czech)
- Joe Hansen <joedalton2@yahoo.dk> (Danish)
- Danishka Navin <danishka@gmail.com> (Sinhala)
- Omar Campagne <ocampagne@gmail.com> (Spanish)
- Américo Monteiro <a_monteiro@netcabo.pt> (Portuguese)
- Clytie Siddall <clytie@riverland.net.au> (Vietnamese)
- Sebastiano Pistore <SebastianoPistore.info@protonmail> (Italian)
- phlostically <phlostically@mailinator.com> (Esperanto)
- Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org> (Romanian)

# Manpage translators

- Dr. Tobias Quathamer <toddy@debian.org> (German)
- Américo Monteiro <a_monteiro@netcabo.pt> (Portuguese)
- David Prévot <taffit@debian.org> (French)
- Baptiste Jammet <baptiste@mailoo.org> (French)
- Sebastiano Pistore <SebastianoPistore.info@protonmail> (Italian)
- Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org> (Romanian)
